@extends('themes.app.pages')

@section('content')
	<div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body"> 
                       <h1 class="card-title">About Me 
                        @if(!$abouts)
                        <a class="btn btn-primary pull-right" href="{{ route('abouts.create') }}" type="button">Add MySelf</a>
                        @else
                        @foreach($abouts as $about)
                         <a class="btn btn-primary pull-right" href="{{ route('abouts.edit',['about' => $about->id]) }}" type="button">Update MySelf</a>
                         @endforeach
                        @endif
                        </h1>                  
                       <div class="clearfix"></div>
                       <hr>
                       @foreach($abouts as $about)
                            {!! $about->about_me !!}<br><hr>
                            <span class="col-6 pull-left">
                               <img src="{{ asset('/images/'.$about->profile_image) }}" width="180" height="200">
                               {{-- <img width="250" height="269" style="border-radius: 50%;" src="{{ asset('public/images/'.$about->profile_image) }}" alt=""/> --}}
                            </span>  
                            <span class="col-6">
                                <ul class="list-icons">
                                    <li><b>Address:</b> {{ $about->address }}</li>
                                    <li><b>Phone:</b> {{ $about->phone }}</li>
                                    <li><b>Email:</b> {{ $about->email }}</li>
                                    <li><b>FaceBook Link:</b><a href="{{ $about->fb }}"> {{ $about->fb }} </a></li>
                                    <li><b>Twitter Link:</b><a href="{{ $about->tw }}"> {{ $about->tw }} </a></li>
                                    <li><b>Linkedin Link:</b><a href="{{ $about->li }}"> {{ $about->li }} </a></li>
                                    <li><b>Github Link:</b><a href="{{ $about->git }}"> {{ $about->git }} </a></li>
                                    <li><b>Bitbucket Link:</b><a href="{{ $about->bitbucket }}"> {{ $about->bitbucket }} </a></li>
                                    <li><b>Google+ Link:</b><a href="{{ $about->google }}"> {{ $about->google }} </a></li>
                                   
                                </ul>
                                
                            </span><br><br>

                            <span class="col-12">
                                <embed width="100%" height="600px" src="/images/{{ $about->cv }}" type="application/pdf">
                            </span> 
                                            
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
@stop 