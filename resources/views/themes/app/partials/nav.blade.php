     <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a href="{{ url('/admin') }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
                        </li>
                        <li> <a href="{{ route('abouts.index') }}" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">About Me </span></a>
                        </li>
                        <li> <a href="{{ route('skills.index') }}" aria-expanded="false"><i class="fa fa-cog"></i><span class="hide-menu">Skills</span></a>
                        </li>
                        <li> <a href="{{ route('educations.index') }}" aria-expanded="false"><i class="fa fa-graduation-cap"></i><span class="hide-menu">Educations</span></a>
                        </li>
                        <li> <a href="{{ route('experiences.index') }}" aria-expanded="false"><i class="fa fa-briefcase"></i><span class="hide-menu">Experiences</span></a>
                        </li>
                        <li> <a href="{{ route('portfolios.index') }}" aria-expanded="false"><i class="fa fa-drivers-license"></i><span class="hide-menu">Portfolio</span></a>
                        </li>
                         <li> <a href="{{ route('testimonials.index') }}" aria-expanded="false"><i class="fa fa-file-word-o"></i><span class="hide-menu">Testimonial</span></a>
                        </li>
                         <li> <a href="{{ route('contacts.index') }}" aria-expanded="false"><i class="fa fa-comment"></i><span class="hide-menu">Message</span></a>
                        </li>
                    </ul>
                </nav>