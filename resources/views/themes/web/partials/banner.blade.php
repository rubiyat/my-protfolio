<div class="banner-info">
	@if($abouts)
	@foreach($abouts as $about)
	<div class="banner-left">
		<img width="250" height="269" style="border-radius: 50%;" src="/images/{{ $about->profile_image }}" alt=""/>
	</div>
	@endforeach
	@endif
	<div class="banner-right">
	@if($users)
	@foreach($users as $user)
		<h1>I’M {{ $user->name }}</h1>
		<div class="border"></div>
	@endforeach
	@endif
	@if($abouts)
	@foreach($abouts as $about)	
		<h2>{{ $about->designation }}</h2>
		
		<a href="/images/{{ $about->cv }}" download>DOWNLOAD MY RESUME</a>
	</div>
	@endforeach
	@endif
	<div class="clearfix"></div>
</div>