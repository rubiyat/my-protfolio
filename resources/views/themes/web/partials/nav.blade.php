	<!--top-nav---->
			
			<div class="top-nav">
				<div class="nav-icon">
					    <a href="#" class="right_bt" id="activator"><span></span> </a>
				</div>
				<div class="box" id="box">
					<div class="box_content">        					                         
						<div class="box_content_center">
							<div class="form_content">
								<div class="menu_box_list">
									<ul>
										<li><a class="scroll" href="#home"><span>home</span></a></li>
										<li><a class="scroll" href="#about"><span>about</span></a></li>
										<li><a class="scroll" href="#services"><span>services</span></a></li>
										<li><a class="scroll" href="#portfolio"><span>portfolio</span></a></li>
										<li><a class="scroll" href="#testimonial"><span>Testimonial</span></a></li>
										{{-- <li><a class="scroll" href="#blog"><span>blog</span></a></li> --}}
										<li><a class="scroll" href="#contact"><span>Contact</span></a></li>
										<div class="clearfix"> </div>
									</ul>
								</div>
								<a class="boxclose" id="boxclose"> <span> </span></a>
							</div>                                  
						</div> 	
					</div> 
				</div>       	  
			</div>