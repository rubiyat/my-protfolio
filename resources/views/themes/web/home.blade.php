@include('themes.web.partials.header')
<!-- banner -->
<div class="banner" id="home">
	<div class="container"> 
		<div class="top-header row">
			<script src="{{ asset('js/web/classie.js') }}"></script>
			<!--top-nav---->
			@include('themes.web.partials.nav')
			<!---start-click-drop-down-menu----->
			        <!----start-dropdown--->
			         <script type="text/javascript">
						var $ = jQuery.noConflict();
							$(function() {
								$('#activator').click(function(){
									$('#box').animate({'top':'0px'},900);
								});
								$('#boxclose').click(function(){
								$('#box').animate({'top':'-1000px'},900);
								});
							});
							$(document).ready(function(){
							//Hide (Collapse) the toggle containers on load
							$(".toggle_container").hide(); 
							//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
							$(".trigger").click(function(){
								$(this).toggleClass("active").next().slideToggle("slow");
									return false; //Prevent the browser jump to the link anchor
							});
												
						});
					</script>
			<!---//End-click-drop-down-menu----->
			<div class="clearfix"> </div>
		</div>
		@include('themes.web.partials.banner')
	</div>
</div>
<!--/header-banner-->
<!--about-->
<div class="about text-center" id="about">
	<div class="container">
		<h3>ABOUT ME</h3>
		@if($abouts)
		@foreach($abouts as $about)
		<div class="strip text-center"><img src="{{ asset('images/about.png') }}" alt=" "/></div>
		<p>{!! $about->about_me !!}</p><br><br>
		
		@if($about->fb)
		<a href="{{ $about->fb }}" class="fa fa-facebook"></a>
		@endif
		@if($about->tw)
		<a href="{{ $about->tw }}" class="fa fa-twitter"></a>
		@endif
		@if($about->li)
		<a href="{{ $about->li }}" class="fa fa-linkedin"></a>
		@endif
		@if($about->google)
		<a href="{{ $about->google }}" class="fa fa-google"></a>
		@endif
		@if($about->git)
		<a href="{{ $about->git }}" class="fa fa-git"></a>
		@endif
		@if($about->bitbucket)
		<a href="{{ $about->bitbucket }}" class="fa fa-bitbucket"></a>
		@endif
		@endforeach
		@endif
	</div>
</div>
<!--//about-->
<div class="about-back"></div>
<!--my skill-->
<div class="my-skills text-center">
	<div class="container">
		<h3>MY SKILLS</h3>
		<div class="strip text-center"><img src="{{ asset('images/skill.png') }}" alt=" "/></div>
		<div class="skill-grids">
			@if($skills)
			@foreach($skills as $key=>$skill)
			<div class="col-md-2 skills-grid text-center">
				<div class="progress-circle progress-{{  $skill->progress }}"><span>{{  $skill->progress }}</span></div>
				<p style="margin-left: 40px;">{{ $skill->name }}</p>
			</div>
			@endforeach
			@endif
				{{-- <div class="progress-circle progress-75"><span>75</span></div>	 --}}
			<div class="clearfix"> </div>
		</div>
	</div>	
</div>
<!--//my skill-->
<div class="my-skill-back"></div>
<!--education-->
<div class="education text-center">
	<div class="container">
		<div class="edu-info">
			<h3>EDUCATION</h3>
		</div>
		<div class="strip text-center"><img src="{{ asset('images/edu.png') }}" alt=" "/></div>
		<div class="edu-grids">
			@if($educations)
			@foreach($educations as $education)
			<div class="col-md-6 edu-grid">
				<p>{{ $education->session }}</p><span>Graduated</span>
				<img class="img-responsive" src="{{ asset('images/arrow.png') }}" alt=""/>
				<div class="edu-border">
					<div class="edu-grid-master">
						<h3>{{ $education->certificate }}</h3>
						<h4>{{ $education->inistitute }}</h4>
					</div>
					<div class="edu-grid-info">
						<h5>{{ $education->result }}</h5>
					</div>
				</div>
			</div>
			@endforeach
			@endif
			<div class="clearfix"></div>
		</div>
	</div>
	
</div>
<!--//education-->
<div class="strip-border"><p></p></div>
<!--work-->
<div class="work-experience text-center">
	
	<div class="container">
		<div class="work-info">
			<h3>WORK EXPERIENCE</h3>
		</div>
		<div class="strip text-center"><img src="{{ asset('images/work.png') }}" alt=" "/></div>
		<div class="work-grids">
			<div class="col-md-4 w-grid">
				@if($experiences)
				@foreach($experiences as $experience)
				<div class="work-grid">
					<h3>{{ $experience->star_date }} - {{ $experience->end_date }}</h3>
					<div class="work-grid-info">
						<h4>{{ $experience->company_name }}</h4>
						<h5>{{ $experience->position }}</h5>
						<p>{!! $experience->description !!}</p>
					</div>
				</div>
				@endforeach
				@endif
			</div>
		
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//work-->
<div class="services-back"></div>

<!--services-->
<div class="services text-center" id="services">
	<div class="container">
		<div class="ser-info">
			<h3>SERVICES</h3>
		</div>
		<div class="strip text-center"><img src="{{ asset('images/ser.png') }}" alt=" "/></div>
		<div class="ser-grids">
			<div class="col-md-3 col-md-offset-2 ser-grid">
				<div class="ser-imagea"></div>
				<h3>Web Design</h3>
				<p>I'm Providing you professional and best attractive responsive web site design. </p>
			</div>
			<div class="col-md-3 ser-grid">
				<div class="ser-imageb"></div>
				<h3>Graphic Design</h3>
				<p>I'm professionally doing logo, banner, poster design with attractive art and creative design.</p>
			</div>
			
			<div class="col-md-3 ser-grid">
				<div class="ser-imaged"></div>
				<h3>Development</h3>
				<p>I'm providing you best solution to software design and development</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//services-->
<!--portfolio-->
<div class="gallery-section text-center" id="portfolio">
	<div class="container">
		<h3>PORTFOLIO</h3>
		<div class="strip text-center"><img src="{{ asset('images/port.png') }}" alt=" "/></div>
		
		<div class="gallery-grids">
			<div class="top-gallery">
				@foreach($portfolios as $portfolio)
				<div style="height:300px;" class="col-md-4 gallery-grid gallery1">
					{{-- <a href="{{ $portfolio->url }}" class="swipebox"><img height="300" src="images/{{ $portfolio->photo }}" class="img-responsive" alt="/"> --}}
					<a href="{{ $portfolio->url }}" class="swipebox"><img height="300" src="{{ asset('public/images/'.$portfolio->photo) }}" class="img-responsive" alt="/">
						<div class="textbox">
							<h4>{{ $portfolio->name }}</h4>
							<p style="color: #000">{{ $portfolio->project_type }}</p>
							<div class="button"><a href="{{ asset('images/p1.jpg') }}" class="swipebox">VIEW</a></div>
						</div>
				</div></a>
				@endforeach
				
				
				
				
			
				<div class="clearfix"> </div>
			</div>
				{{-- <link rel="stylesheet" href="{{ asset('css/web/swipebox.css') }}">
				<script src="{{ asset('js/web/jquery.swipebox.min.js') }}"></script> 
					<script type="text/javascript">
						jQuery(function($) {
							$(".swipebox").swipebox();
						});
					</script> --}}
		</div>
	</div>
</div>
<!--//portfolio-->
<!--process-->
<div class="process text-center">
	<div class="container">
		<h3>PROCESS</h3>
		<div class="strip text-center"><img src="{{ asset('images/pro.png') }}" alt=" "/></div>
		<div class="process-girds">
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imagea"></div>
					<p>IDEA</p>
					<img src="{{ asset('images/6.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imageb"></div>
					<p>CONCEPT</p>
					<img class="pro-imga" src="{{ asset('images/6.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imagec"></div>
					<p>DESIGN</p>
					<img class="pro-img" src="{{ asset('images/6.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imaged"></div>
					<p>DEVELOP</p>
					<img class="pro-imgb" src="{{ asset('images/6.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imagee"></div>
					<p>TEST</p>
					<img src="{{ asset('images/6.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-2 process-pad">
				<div class="process-gird">
					<div class="process-imagef"></div>
					<p>LAUNCH</p>
					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//process-->
<div class="process-back"></div>
<!--testimonials-->
<div class="testimonials" id="testimonial">
	<div class="container">
		<h3>TESTIMONIAL</h3>
		<div class="strip text-center"><img src="{{ asset('images/test.png') }}" alt=" "/></div>
			<!-- responsiveslides -->
					<script src="{{ asset('js/web/responsiveslides.min.js') }}"></script>
						<script>
							// You can also use "$(window).load(function() {"
							$(function () {
							 // Slideshow 4
							$("#slider3").responsiveSlides({
								auto: true,
								pager: false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
							$('.events').append("<li>before event fired.</li>");
							},
							after: function () {
								$('.events').append("<li>after event fired.</li>");
								}
								});
								});
						</script>
			<!-- responsiveslides -->
			<div  id="top" class="callbacks_container">
					<ul class="rslides" id="slider3">
						@if($testimonials)
						@foreach($testimonials as $testimoal)
						@if($testimoal->status == 0)
						<li>
							<div class="test-banner">
								<img class="quoa" src="{{ asset('images/quo2.png') }}" alt=""/>
								<div class="test-left">
									{{-- <img style="width:200px;height:200;border-radius: 50%;" src="images/{{ $testimoal->image }}" alt=""/> --}}
									<img style="width:200px;height:200;border-radius: 50%;" src="{{ asset('public/images/'.$testimoal->image) }}" alt=""/>
								</div>
								<div class="test-right">
									<p>{{ $testimoal->description }}</p>
									<h4>{{ $testimoal->name }}</h4>
								</div>
								<div class="clearfix"></div>
								<img class="quo" src="{{ asset('images/quo1.png') }}" alt=""/>
							</div>
						</li>
						@endif
						@endforeach
						@endif
						
					</ul>
			</div>
			
	</div>
</div>
<!--testimonials-->
<!--clients-->
{{-- <div class="clients text-center">
	<div class="container">
		<h4>Clients</h4>
		<div class="strip-line"></div>
		<div class="client-grids">
			<div class="col-md-4 cl-grid">
				<div class="client-grid">
					<img src="{{ asset('images/10.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-4 cl-grid">
				<div class="client-grid">
					<img src="{{ asset('images/11.png') }}" alt=""/>
				</div>
			</div>
			<div class="col-md-4 cl-grid">
				<div class="client-grid">
					<img src="{{ asset('images/12.png') }}" alt=""/>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div> --}}
<!--clients-->
{{-- <div class="client-back"></div>
 --}}<!--blog-->
{{-- <div class="blog" id="blog">
	<div class="container">
		<div class="blog-info text-center">
			<h3>BLOG</h3>
			<div class="strip text-center"><img src="{{ asset('images/blog.png') }}" alt=" "/></div>
		</div>
		<div class="blog-grids">
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/a.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>	
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/b.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/c.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/a.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/b.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-text-info">
				<div class="blog-grid">
					<a href="single.html"><img src="{{ asset('images/c.jpg') }}" alt=""/></a>
					<div class="blog-text">
						<a href="single.html">Nature is lethal but it doesn't hold a candle to man.</a>
						<div class="stripa"></div>
						<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria 
						and viruses differently than mine. You don't get sick, I do.</p>
					</div>
				
					<div class="blog-para">
							<ul>
								<li><a href="#">3 Comments</a></li>
								<li><a href="#">8 Share</a></li>
							</ul>
					</div>
					<div class="blog-pos">
						<p>5<span>May</span></p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div> --}}
<!--//blog-->
{{-- <div class="contact-back"></div> --}}
<!--contact-->
<div class="contact" id="contact">
	<div class="container">
		<div class="contact-info text-center">
			<h3>CONTACT</h3>
			<div class="strip text-center"><img src="{{ asset('images/con1.png') }}" alt=" "/></div>
		</div>
		<div class="contact-grids">
			<div class="col-md-5 contact-left">
				<h3>Say Hi to Me</h3>
				<div class="stripb"></div>
				<ul>
					@foreach($abouts as $about)
					<li>{{ $about->address }}</li>
					<li>{{ $about->phone }}</li>
					<li><a href="mailto:{{ $about->email }}">rubiyat21@gmail.com</a></li>
					@endforeach
				</ul>
			</div>
			<div class="col-md-7 contact-right">
				<h3>Drop Me A Line</h3>
				<div class="stripb"></div>
				<form action="{{ route('contacts') }}" method="post">
					{{ csrf_field() }}
					<input type="text" value="Name" name="name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
					<input type="text" value="E-mail" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-mail';}" required="">
					<textarea type="text" name="message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}" required="">Message</textarea>
					<input type="submit" value="SEND">
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//contact-->
<div class="footer-top"></div>
<!--resume-->
<div class="resume text-center">
	<div class="container">
		<div class="strip text-center"><a href="#"><img src="{{ asset('images/down.png') }}" alt=" "/></a></div>
		<div class="down"><a href="/images/{{ $about->cv }}">Download My Resume</a></div>
	</div>
</div>
<!--//resume-->

<!--footer-->
<div class="footer">
	<div class="container">
		<p>Copyright &copy; 2018 . <a href="#"> Rubiyat Hasan Siddik</a></p>
	</div>
</div>
<!--//footer-->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>